# Bridge Inference Service

This Flask-based web service provides an interface to upload and process images using a pre-trained machine learning model. It is primarily designed to run inference tasks on uploaded images, display the results, and provide status updates for the ongoing tasks.

## Run

If you have docker installed:

```bash
# Pull
docker pull bnetbutter/bridgeinference-flask-service:latest

# Run if weights on machine before build
docker run -p 5000:5000 bnetbutter/bridgeinference-flask-service:latest

# Run if weights not on machine before build
docker run -p 5000:5000 -v path/to/weights/folder:/app/bridge-inference/weights bnetbutter/bridgeinference-flask-service:latest
```

Then open a browser, navigate to `http://localhost:5000` to upload pictures for inference

---

## Important
This repo does not contain the .pth weights. Download it directly from CII's Microsoft Teams drive. This is due to gitlab's size limit of 100MB per file. We are working on finding ways to host the weights so it can be downloaded directly.

For now, however, the docker image will contain the weights and so inference can be done using the docker container.

The weights can be found at `Documents/General/BridgeNet/weights.pth` in the Microsoft Teams drive. **TODO:** version control for the weights.

The container will throw an error if there no weights folder exists on your machine. There are two ways around this.

1. **Before** building the container, ensure `bridge-inference/weights/weights.pth` exists.

2. Make the weights directory anywhere on your local machine. Use the `-v` command to mount the folder inside of the docker container at
`/app/bridge-inference/weights`.

---

## Key Features:

1. **File Upload Interface (`GET /`):** The root endpoint of the service provides an HTML-based interface for users to upload image files.

2. **File Upload Processing (`POST /upload`):** This endpoint allows users to upload one or more images to the server. The uploaded images are processed and prepared for an inference job. Each upload job is assigned a unique identifier.

3. **Inference (`GET /infer/<uuid_dir>`):** This endpoint initiates an inference job on the uploaded images associated with the given UUID. It runs the inference job in a separate worker thread, allowing the function to return a response quickly and the client to check the status of the job at a later time.

4. **Job Status (`GET /status/<uuid>`):** This endpoint provides the status of the inference job associated with the given UUID. It can be used to track the progress of the job and get the results of the completed inference.

5. **Image Display (`GET /display_images/<uuid>`):** This endpoint generates an HTML page displaying all the inference results (images) associated with the given UUID.

6. **Image Serving (`GET /images/<uuid>/<filename>`):** This endpoint serves individual image files from the inference results.
## Route: `GET /`

### Function: `index()`

This is the root endpoint of the application. It renders and returns an HTML template named `upload.html`, 
which contains a form that allows user to upload images. This is mainly for debug
purposes. Use the `/upload` route defined below for API access

## Route: `POST /upload`

### Function: `upload_file()`

This endpoint allows for uploading multiple files to the server. These files are then processed and prepared for an inference job. A unique directory is created for each POST request to hold the uploaded files.


### Parameters:

- `files[]` (form data): A list of one or more files to be uploaded to the server. Files must be of a type that is allowed by the server configuration (validation done by the `allowed_file()` function).

### Returns:

- Redirects to the `infer` route with the unique directory (`uuid_dir`) as an argument.

---

## Route: `GET /infer/<uuid_dir>`

### Function: `infer(uuid_dir)`

This endpoint performs an inference task on the images uploaded to the `uuid_dir` directory. The function loads the images, runs the inference using a trained model, stores the inference results, and updates the status of the job.

---

### Parameters:

- `uuid_dir` (URL parameter): The unique directory identifier where the uploaded images for inference are stored.

### Returns:

- Redirects to the `status` route with the unique directory (`uuid_dir`) as an argument.

### Process:

1. The function first sets up a DataLoader with the images in the `uuid_dir` directory.
2. It creates an `inference_dir` directory inside a configured inference folder, if it doesn't exist already.
3. It collects the paths of all the images that need to be processed, excluding .json files.
4. A worker thread is started to run the inference on each image:
    - For each image, the inference result is calculated using the trained model.
    - If the model found something (e.g., certain objects), it saves the result (both image and mask) and updates the status of the job.
    - Each detected object's label, bounding box, and mask polygon are stored in the job status.
5. The function then redirects to the `status` route with the unique directory (`uuid_dir`) as an argument.

---

## Route: `GET /status/<uuid>`

### Function: `get_status(uuid)`

This endpoint is used to check the status of the inference job associated with a specific `uuid`. The status of the job is stored in the `inference_status` dictionary in memory.

---

### Parameters:

- `uuid` (URL parameter): The unique identifier associated with a specific inference job.

### Returns:

- A JSON object containing the status of the job. The status can be "Not Started" if the job has not been initiated, or it could be the status dictionary associated with the job in the `inference_status` dictionary.

```json
{
  "status": "Not Started"
}
```

or

```json
{
  "status": {
    "image1.jpg": {
      "filename": "image1.jpg",
      "status": "Done",
      "result": {
        "label": "pier",
        "box": [50, 50, 200, 200],
        "polygons": [
            [50, 50],
            [200, 200]
        ]
      }
    },
    "image2.jpg": {
      "filename": "image2.jpg",
      "status": "pending",
      "result": {}
    }
  }
}

```

## Route: `GET /display_images/<uuid>`

### Function: `display_images(uuid)`

This endpoint is used to display all the images from the inference job associated with a specific `uuid`. It generates an HTML response with embedded `img` tags for each image.

---

### Parameters:

- `uuid` (URL parameter): The unique identifier associated with a specific inference job.

### Returns:

- An HTML response containing `img` tags for each image. Each `img` tag uses the `/images/<uuid>/<filename>` route as its source.

---

## Route: `GET /images/<uuid>/<filename>`

### Function: `send_image(uuid, filename)`

This endpoint is used to serve individual image files. The images are located in the `inferences/<uuid>` directory.

---

### Parameters:

- `uuid` (URL parameter): The unique identifier associated with a specific inference job.
- `filename` (URL parameter): The name of the image file to be served.

### Returns:

- The requested image file. If the file is not found, a 404 error will be returned.
