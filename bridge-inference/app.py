import os
import sys

# add mask-rcnn submodule to python path so we can import it
sys.path.append(
    os.path.join(os.path.dirname(os.path.abspath(__file__)), "pytorch-mask-rcnn")
)

import pytorch_mask_rcnn as pmr
import torch
import json
import numpy as np


USE_CUDA = True
DATASET = "coco"
CKPT_PATH = "weights/weights.pth"
DATA_DIR = "bridge-data"

# Set the device to CUDA if gpu is available
device = torch.device("cuda" if torch.cuda.is_available() and USE_CUDA else "cpu")
if device.type == "cuda":
    pmr.get_gpu_prop(show=True)
print("\ndevice: {}".format(device))


# Load the dataset that the model was trained with. We need to make sure that
# the number of classes match the one with weights

ds = pmr.datasets(DATASET, DATA_DIR, "test", train=False)
d = torch.utils.data.DataLoader(ds, shuffle=False)

model = pmr.maskrcnn_resnet50(True, max(ds.classes) + 1).to(device)
model.eval()
model.head.score_thresh = 0.7

if CKPT_PATH:
    checkpoint = torch.load(CKPT_PATH, map_location=device)
    model.load_state_dict(checkpoint["model"])
    print(checkpoint["eval_info"])
    del checkpoint


# We need the annotation data to do lookups
with open(os.path.join(DATA_DIR, "train", "_annotations.coco.json")) as fp:
    ANNOTATION_TABLE = json.load(fp)


def get_label_from_id(label_id):
    return ANNOTATION_TABLE["categories"][label_id].copy()

# Gonna put flask and the pytorch model in the same address space so we save
# time loading weights and stuffs

from flask import (
    Flask,
    request,
    redirect,
    url_for,
    render_template,
    jsonify,
    send_from_directory,
)
import os
import uuid
import json
import cv2
import threading


app = Flask(__name__)

UPLOAD_FOLDER = "uploaded_images"
INFERENCE_FOLDER = "inferences"
ALLOWED_EXTENSIONS = {"png", "jpg", "jpeg", "gif"}

app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER
app.config["INFERENCE_FOLDER"] = INFERENCE_FOLDER


def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS


# Index route returns a html form that allows user to upload multiple images
# It's primarily for debugging purposes.
@app.route("/")
def index():
    return render_template("upload.html")


inference_status = {}  # Store the result of jobs


@app.route("/status/<uuid>", methods=["GET"])
def get_status(uuid):
    status = inference_status.get(uuid, "Not Started")
    return jsonify({"status": status})


# Upload images via POST request. This is the primary entry point for this
# service
@app.route("/upload", methods=["POST"])
def upload_file():
    if "files[]" not in request.files:
        return redirect(request.url)

    files = request.files.getlist("files[]")
    UUID = str(uuid.uuid4())

    # Create a unique directory for this POST request
    unique_dir = os.path.join(app.config["UPLOAD_FOLDER"], UUID)
    os.makedirs(unique_dir)

    for file in files:
        if file and allowed_file(file.filename):
            filename = file.filename
            file.save(os.path.join(unique_dir, filename))

    uuid_dir = os.path.basename(unique_dir)
    uuid_path = unique_dir

    # Load the annotation file from the training set. We really just care
    # about the segmentation classes
    with open("bridge-data/train/_annotations.coco.json") as fp:
        annotation = json.load(fp)

    # Don't need the annotations that goes with the specific images
    del annotation["annotations"]

    # We also don't need the images from the image directory, but we do need
    # to populate the images with the images we just uploaded
    images = annotation["images"] = []
    job_status = inference_status[UUID] = {}
    for img_path in os.listdir(uuid_path):
        image = cv2.imread(os.path.join(uuid_path, img_path))
        height, width, _ = image.shape
        images.append(
            {
                "id": len(images),
                "file_name": img_path,
                "width": width,
                "height": height,
            }
        )
        job_status[img_path] = {"filename": img_path, "status": "pending", "result": {}}

    # Create '_annotations.coco.json' file
    with open(os.path.join(uuid_path, "_annotations.coco.json"), "w") as f:
        json.dump(annotation, f, indent=2)

    return redirect(url_for("infer", uuid_dir=uuid_dir))


# Once the images are uploaded, we will run mask rcnn to extract features
@app.route("/infer/<uuid_dir>", methods=["GET"])
def infer(uuid_dir):
    dataset = pmr.datasets(DATASET, app.config["UPLOAD_FOLDER"], uuid_dir, train=False)
    d = torch.utils.data.DataLoader(dataset, shuffle=False)
    inference_dir = os.path.join(app.config["INFERENCE_FOLDER"], uuid_dir)
    if not os.path.exists(inference_dir):
        os.makedirs(inference_dir)

    image_paths = [
        img
        for img in os.listdir(os.path.join(app.config["UPLOAD_FOLDER"], uuid_dir))
        if not img.endswith(".json")
    ]

    # Let the inference run in its own thread
    # This way, the route can return a response right away and the client
    # can query the status of the job later
    def worker():
        with torch.no_grad():
            for (image, target), name in zip(d, image_paths):
                image = image.to(device)[0]
                result = model(image)
                
                # We found something interesting...
                if len(result["labels"]):
                    # TODO add a link to inference result
                    # TODO save the model result with mask and label information
                    pmr.show(
                        image,
                        result,
                        ds.classes,
                        save_path=os.path.join(inference_dir, name),
                    )
                    
                    # result["labels"] returns a list of primary keys that refers
                    # to the segmentation class. So we need to do a lookup                
                    status = inference_status[uuid_dir][name]
                    status["status"] = "Done"
                    labels = status["labels"] = []

                    # Iterate over each semantic instance found in this image
                    for box, mask, id in zip(result["boxes"], result["masks"], result["labels"]):
                        label = get_label_from_id(id)
                        label["box"] = box.tolist()

                        # Extract polygon from mask data
                        if mask.is_floating_point():
                            mask = mask > 0.5
                        m = np.asarray(mask.cpu())
                        polygon = pmr.GenericMask(m, image.shape[1], image.shape[0]).polygons
                        label["polygons"] = [seg.reshape(-1, 2).tolist() for seg in polygon]
                        labels.append(label)

    thrd = threading.Thread(target=worker)
    thrd.start()

    return redirect(url_for("status", uuid=uuid_dir))


@app.route("/status/<uuid>", methods=["GET"])
def status(uuid):
    status = inference_status.get(uuid, "Not Started")
    return jsonify(status)


@app.route("/display_images/<uuid>", methods=["GET"])
def display_images(uuid):
    image_names = os.listdir(os.path.join("inferences", uuid))
    image_files = [
        f"""<img src="/images/{uuid}/{image_name}" alt="{image_name}">"""
        for image_name in image_names
    ]

    return "<br>".join(image_files)


@app.route("/images/<uuid>/<filename>")
def send_image(uuid, filename):
    return send_from_directory(os.path.join("inferences", uuid), filename)


if __name__ == "__main__":
    for path in {UPLOAD_FOLDER, INFERENCE_FOLDER}:
        if not os.path.exists(path):
            os.makedirs(path)
    app.run()
